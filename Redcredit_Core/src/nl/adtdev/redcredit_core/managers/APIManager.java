package nl.adtdev.redcredit_core.managers;

import java.util.Arrays;
import java.util.Scanner;

import nl.adtdev.redcredit_core.Redcredit;

public class APIManager {

	public Redcredit plugin;
	
	public APIManager(Redcredit p)
	{
		plugin = p;
	}
	
	public void setCredits(String path, int value)
	{
		String[] data = plugin.cmanager.getValueList(path).toArray(new String[plugin.cmanager.getValueList(path).size()]);
		
		Scanner sc1 = new Scanner(data[0]).useDelimiter("[^0-9]+");
		int rcid = sc1.nextInt();
		
		String[] data2 = {"ID: " + rcid, "Credits: " + value};
		
		this.plugin.cmanager.setValueList(path, Arrays.asList(data2));
		
		updateCredits(path);
	}
	
	@SuppressWarnings("resource")
	public void updateCredits(String path)
	{
		String[] data = plugin.cmanager.getValueList(path).toArray(new String[plugin.cmanager.getValueList(path).size()]);
		
		Scanner sc2 = new Scanner(data[1]).useDelimiter("[^0-9]+");
		int credits = sc2.nextInt();
		
		plugin.smanager.updateCredits(credits);
	}
	
}
