package nl.adtdev.redcredit_core.managers;

import java.util.Arrays;
import java.util.Scanner;

import nl.adtdev.redcredit_core.Redcredit;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandManager implements CommandExecutor {
	
	public Redcredit plugin;

	public CommandManager(Redcredit p){
		this.plugin = p;
	}

	@SuppressWarnings({ "resource", "deprecation" })
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		boolean b = false;
		
		if(cmd.getName().equalsIgnoreCase("redcredit")){
			if(sender instanceof Player){
				if(args.length > 0){
					for (Player p: plugin.getServer().getOnlinePlayers()) {
						if(p.getName().equals(args[0]))
						{
							b = true;
						}
					}
					
					if(args[0] != null && args[1] != null && b){
						if(args[1].equalsIgnoreCase("setCredit")){
							Scanner in = new Scanner(args[2]).useDelimiter("[^0-9]+");
							int value = in.nextInt();
							
							if(args[2] != null && value > 0){
								String path = "users." + args[0] + ".data";
								
								String[] data = plugin.cmanager.getValueList(path).toArray(new String[plugin.cmanager.getValueList(path).size()]);
								
								Scanner sc1 = new Scanner(data[0]).useDelimiter("[^0-9]+");
								int rcid = sc1.nextInt();
								
								String[] data2 = {"ID: " + rcid, "Credits: " + value};
								
								this.plugin.cmanager.setValueList(path, Arrays.asList(data2));
								plugin.apimanager.updateCredits(path);
								
								sender.sendMessage(ChatColor.GREEN + "Set the value of " + args[0] + " to " + ChatColor.RED + "R" + value + ChatColor.GREEN + "!");

							}
							else
							{
								sender.sendMessage(ChatColor.RED + "The value needs to be higher then 0!");
							}
						}
						else if(args[1].equalsIgnoreCase("getCredit")){
							String path = "users." + args[0] + ".data";
							
							String[] data = plugin.cmanager.getValueList(path).toArray(new String[plugin.cmanager.getValueList(path).size()]);
							
							Scanner sc1 = new Scanner(data[1]).useDelimiter("[^0-9]+");
							int credits = sc1.nextInt();
							
							sender.sendMessage(ChatColor.GREEN + "Player: " + args[0] + " has " + ChatColor.RED + "R" + credits + ChatColor.GREEN + "!");
						}
						else if(args[1].equalsIgnoreCase("addCredit")){
							Scanner in = new Scanner(args[2]).useDelimiter("[^0-9]+");
							int value = in.nextInt();
							
							if(args[2] != null && value > 0){
								String path = "users." + args[0] + ".data";

								String[] data = plugin.cmanager.getValueList(path).toArray(new String[plugin.cmanager.getValueList(path).size()]);
								
								Scanner sc1 = new Scanner(data[0]).useDelimiter("[^0-9]+");
								int rcid = sc1.nextInt();
								
								Scanner sc2 = new Scanner(data[1]).useDelimiter("[^0-9]+");
								int credits = sc2.nextInt() - value;
								
								String[] data2 = {"ID: " + rcid, "Credits: " + credits};
								
								this.plugin.cmanager.setValueList(path, Arrays.asList(data2));
								plugin.apimanager.updateCredits(path);
								
								sender.sendMessage(ChatColor.GREEN + "Gave, " + args[0] + "," + ChatColor.RED + " R" + value + ChatColor.GREEN + ", current balance: " + ChatColor.RED + "R" + credits + ChatColor.GREEN + "!");
							}
							else
							{
								sender.sendMessage(ChatColor.RED + "The value needs to be higher then 0!");
							}
						}
						else if(args[1].equalsIgnoreCase("takeCredit")){
							Scanner in = new Scanner(args[2]).useDelimiter("[^0-9]+");
							int value = in.nextInt();
							
							if(args[2] != null && value > 0){
								String path = "users." + args[0] + ".data";

								String[] data = plugin.cmanager.getValueList(path).toArray(new String[plugin.cmanager.getValueList(path).size()]);
								
								Scanner sc1 = new Scanner(data[0]).useDelimiter("[^0-9]+");
								int rcid = sc1.nextInt();
								
								Scanner sc2 = new Scanner(data[1]).useDelimiter("[^0-9]+");
								int credits = sc2.nextInt() - value;
								
								String[] data2 = {"ID: " + rcid, "Credits: " + credits};
								
								this.plugin.cmanager.setValueList(path, Arrays.asList(data2));
								plugin.apimanager.updateCredits(path);
								
								sender.sendMessage(ChatColor.GREEN + "Took " + ChatColor.RED + "R" + value + ChatColor.GREEN + "from" + args[0] + ", current balance: " + ChatColor.RED + "R" + credits + ChatColor.GREEN + "!");
							}
							else
							{
								sender.sendMessage(ChatColor.RED + "The value needs to be higher then 0!");
							}
						}
						ConfigManager.saveUsersFile();
						return true;
					}
					return true;
				}
			}
		}
		return false;
	}

}